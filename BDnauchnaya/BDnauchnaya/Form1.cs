﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Npgsql;

namespace BDnauchnaya
{
    public partial class Form1 : Form
    {
        Graphics g;
        string conn_param = "Server=127.0.0.1;Port=5432;User Id=postgres;Password=1234;Database=texts;";
        List<string> result = new List<string>();
        List<string[]> all_words = new List<string[]>();
        List<string[]> all_words2 = new List<string[]>();
        List<int> count_word = new List<int>();
        Dictionary<string, int> count_words = new Dictionary<string, int>();
        Dictionary<string, int> count_words1 = new Dictionary<string, int>();
        Pen blue = new Pen(Color.Blue);
        Pen red = new Pen(Color.Red);
        bool word = false;
        bool words = false;
        public Form1()
        {
            InitializeComponent();
            g = pictureBox1.CreateGraphics();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (word == false)
            {
                g.Clear(Color.White);
                NpgsqlConnection conn = new NpgsqlConnection(conn_param);
                NpgsqlCommand com = new NpgsqlCommand("select reorder_tsvector1(to_tsvector('russian', textt)) from texts", conn);
                conn.Open();
                NpgsqlDataReader reader;

                result.Clear();
                reader = com.ExecuteReader();
                while (reader.Read())
                {

                    try
                    {
                        result.Add(reader.GetString(0));//Получаем значение из второго столбца! Первый это (0)!
                    }
                    catch (Exception ex) { Console.WriteLine(ex); }
                }

                for (int i = 0; i < result.Count; i++)
                {
                    result[i] = result[i].Replace("'", ""); //убираем кавычки из каждого слова
                    all_words.Add(result[i].Split(','));
                }

                conn.Close();
                counting();
                painting();
                word = true;
            }
            else
            {
                MessageBox.Show("Рассчёты по словам уже проведены");
            }
        }

        void counting()
        {
            string str;
            for (int i = 0; i < all_words.Count; i++)
            {
                for (int j = 0; j < all_words[i].Length; j++)
                {

                    str = all_words[i][j];
                    if (count_words.ContainsKey(str))
                    {
                        count_words[str] += 1;
                    }
                    else
                    {
                        count_words.Add(str, 1);
                    }

                }
            }
            string sql = "insert into words(word, word_count) values";
            NpgsqlConnection conn1 = new NpgsqlConnection(conn_param);
            conn1.Open();
            foreach (var key in count_words.Keys)
            {
                sql += "('" + key + "', " + count_words[key] + "), ";
            }
            sql = sql.Substring(0, sql.Length - 2);
            NpgsqlCommand com1 = new NpgsqlCommand(sql, conn1);
            NpgsqlDataReader reader;
            reader = com1.ExecuteReader();
            conn1.Close();
            //foreach (var key in count_words.Keys)
            //{
            //    Console.WriteLine("{0} : {1}", key, count_words[key]);
            //}
        }

        void painting()
        {
            NpgsqlConnection conn2 = new NpgsqlConnection(conn_param);
            string sql = "select word_count from words order by word_count desc";
            NpgsqlCommand com2 = new NpgsqlCommand(sql, conn2);
            conn2.Open();
            NpgsqlDataReader reader;
            reader = com2.ExecuteReader();
            count_word.Clear();
            while (reader.Read())
            {

                try
                {
                    count_word.Add(reader.GetInt32(0));//Получаем значение из второго столбца! Первый это (0)!
                }
                catch (Exception ex) { Console.WriteLine(ex); }

            }

            float d = 846f / count_word.Count;
            float dd = 0;
            float d1 = 499f / count_word[0];

            for (int i = 0; i < count_word.Count - 1; i++)
            {
                g.DrawLine(blue, dd, 499 - d1 * count_word[i], dd + d, 499 - d1 * count_word[i + 1]);
                dd += d;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            NpgsqlConnection conn = new NpgsqlConnection(conn_param);
            conn.Open();
            string sql = "TRUNCATE words, words1";
            NpgsqlCommand com = new NpgsqlCommand(sql, conn);
            NpgsqlDataReader reader;
            reader = com.ExecuteReader();
            conn.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (words == false)
            {
                g.Clear(Color.White);
                NpgsqlConnection conn = new NpgsqlConnection(conn_param);
                NpgsqlCommand com = new NpgsqlCommand("select reorder_tsvector1(to_tsvector('russian', ff::text)) from (SELECT regexp_split_to_table(textt, '[.!?]') from texts) ff", conn);
                conn.Open();
                NpgsqlDataReader reader;
                reader = com.ExecuteReader();
                result.Clear();
                while (reader.Read())
                {

                    try
                    {
                        result.Add(reader.GetString(0));//Получаем значение из второго столбца! Первый это (0)!
                    }
                    catch (Exception ex) { Console.WriteLine(ex); }
                }

                for (int i = 0; i < result.Count; i++)
                {
                    result[i] = result[i].Replace("'", ""); //убираем кавычки из каждого слова
                    all_words2.Add(result[i].Split(','));
                }

                //for (int i = 0; i < all_words2.Count; i++)
                //{
                //    for (int j = 0; j < all_words2[i].Length; j++)
                //    {
                //        Console.WriteLine(all_words2[i][j]);
                //    }
                //}

                conn.Close();
                counting1();
                painting1();
                words = true;
            }
            else
            {
                MessageBox.Show("Рассчёты по словосочетаниям уже проведены");
            }

        }

        void counting1()
        {
            string str;
            for (int i = 0; i < all_words2.Count; i++)
            {
                for (int j = 0; j < all_words2[i].Length - 1; j++)
                {
                    for (int k = j + 1; k < all_words2[i].Length; k++)
                    {
                        str = all_words2[i][j] + " " + all_words2[i][k];
                        if (count_words1.ContainsKey(str))
                        {
                            count_words1[str] += 1;
                        }
                        else
                        {
                            count_words1.Add(str, 1);
                        }
                    }
                }
            }
            string sql = "insert into words1(word, word_count) values";
            NpgsqlConnection conn1 = new NpgsqlConnection(conn_param);
            conn1.Open();
            foreach (var key in count_words1.Keys)
            {
                sql += "('" + key + "', " + count_words1[key] + "), ";
            }
            sql = sql.Substring(0, sql.Length - 2);
            NpgsqlCommand com1 = new NpgsqlCommand(sql, conn1);
            NpgsqlDataReader reader;
            reader = com1.ExecuteReader();
            conn1.Close();
            //foreach (var key in count_words1.Keys)
            //{
            //    Console.WriteLine("{0} : {1}", key, count_words1[key]);
            //}
        }

        void painting1()
        {
            NpgsqlConnection conn2 = new NpgsqlConnection(conn_param);
            string sql = "select word_count from words1 order by word_count desc";
            NpgsqlCommand com2 = new NpgsqlCommand(sql, conn2);
            conn2.Open();
            NpgsqlDataReader reader;
            reader = com2.ExecuteReader();
            count_word.Clear();
            while (reader.Read())
            {

                try
                {
                    count_word.Add(reader.GetInt32(0));//Получаем значение из второго столбца! Первый это (0)!
                }
                catch (Exception ex) { Console.WriteLine(ex); }

            }

            float d = 846f / count_word.Count;
            float dd = 0;
            float d1 = 499f / count_word[0];

            for (int i = 0; i < count_word.Count - 1; i++)
            {
                g.DrawLine(red, dd, 499 - d1 * count_word[i], dd + d, 499 - d1 * count_word[i + 1]);
                dd += d;
            }
        }

    }
}
